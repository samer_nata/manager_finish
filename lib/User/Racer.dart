class Racer {
  int id;
  String first_name, division;
  String last_name, club;
  int pip;

  Racer(
      {this.id,
      this.first_name,
      this.division,
      this.last_name,
      this.club,
      this.pip});

  factory Racer.fromJSONserver(json) {
    return Racer(
      id: int.parse(json["id"]),
      first_name: json["first_name"],
      division: json["division"],
      last_name: json["last_name"],
      club: json["club"],
      pip: int.parse(json["pip"]),
    );
  }
  factory Racer.fromJSON(json) {
    return Racer(
      id: json["id"],
      first_name: json["first_name"],
      division: json["division"],
      last_name: json["last_name"],
      club: json["club"],
      pip: json["pip"],
    );
  }


  Map<String, dynamic> to_Map() => {
        'id': id,
        'first_name': first_name,
        'division': division,
        'last_name': last_name,
        'club': club,
        'pip': pip
      };

  @override
  String toString() {
    return 'Racer{id: $id, first_name: $first_name, division: $division, last_name: $last_name, club: $club, pip: $pip}';
  }
}
