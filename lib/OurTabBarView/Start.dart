import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:manager_finish/Loop/Loop.dart';
import 'package:manager_finish/Loop/LoopInfo.dart';
import 'package:manager_finish/Race/Race.dart';

import 'package:toast/toast.dart';

import '../Database.dart';

class Start extends StatefulWidget {
  Race race;

  Start(this.race);

  @override
  _StartState createState() => _StartState(race);
}

class _StartState extends State<Start> {
  Race race;
  ScrollController _scrollController;

  _StartState(this.race);

  @override
  void initState() {
    _scrollController = ScrollController();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(child: loopsListView());
  }

  FutureBuilder<List<Loop>> loopsListView() {
    return FutureBuilder(
      initialData: [],
      future: DBProvider.db.Loops_list(race.id),
      builder: (BuildContext context, AsyncSnapshot<List<Loop>> snapshot) {
        if (snapshot.hasData && snapshot.data.length > 0) {
          snapshot.data.sort((a,b) => a.date_start.compareTo(b.date_start));
          return ListView.builder(
              itemCount: snapshot.data.length,
              controller: _scrollController,
              itemBuilder: (BuildContext context, int index) {
                Loop item = snapshot.data[index];

                return Padding(
                  padding: const EdgeInsets.all(1.0),
                  child: Container(
                    color: Color(0xff121214),
                    height: 90,
                    child: Stack(
                      children: <Widget>[
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Container(
                                      width: 275,
                                      child: InkWell(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              new MaterialPageRoute(
                                                  builder: (context) =>
                                                      LoopInfo(item, race)));
                                        },
                                        child: Text(
                                          item.name,
                                          style: TextStyle(
                                              decoration:
                                                  TextDecoration.underline,
                                              color: Colors.white,
                                              fontSize: 15,
                                              fontWeight: FontWeight.w100),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, top: 2.0),
                                    child: Container(
                                      child: Row(
                                        children: <Widget>[
                                          Text(
                                            "Start ${item.date_start.substring(10, 16)} ,",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15,
                                                fontWeight: FontWeight.w100),
                                          ),
                                          Text(
                                            "  ${item.total_distance} km",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15,
                                                fontWeight: FontWeight.w100),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                              padding: const EdgeInsets.only(right: 16.0),
                              child: item.is_start
                                  ? Text(
                                      "Tävlingar har startat",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15,
                                          fontWeight: FontWeight.w100),
                                    )
                                  : InkWell(
                                      onTap: () => startLoop(item),
                                      child: Container(
                                        width: 80,
                                        height: 40,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10)),
                                            gradient: LinearGradient(
                                                colors: <Color>[
                                                  Colors.orange,
                                                  Colors.orange,
                                                  Colors.orange,
                                                  Colors.orange,
                                                  Colors.deepOrange,
                                                  Colors.deepOrange
                                                ],
                                                begin: Alignment.topLeft,
                                                end: Alignment.bottomRight)),
                                        child: Center(
                                          child: Text(
                                            "Start",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15,
                                                fontWeight: FontWeight.w300),
                                          ),
                                        ),
                                      ),
                                    )),
                        ),
                      ],
                    ),
                  ),
                );
              });
        } else if (snapshot == null) {
          return Center(
              child: SpinKitThreeBounce(
            color: Colors.blue,
            size: 17,
          ));
        } else if (snapshot.data.length == 0) {
          Center(
              child: Text(
            "No Loops Here",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300),
          ));
        }
        return Center(
          child: CircularProgressIndicator(
            backgroundColor: Colors.green,
          ),
        );
      },
    );
  }

  void startLoop(Loop loop) {
    loop.is_start=true;
    DBProvider.db.update_loops(loop, loop.id);
    setState(() {

    });
  }
}
