import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:manager_finish/Race/Race.dart';
import 'package:manager_finish/User/Racer.dart';

import '../Database.dart';
import '../Link.dart';
import 'LapsTimes.dart';
import 'Loop.dart';
import 'package:http/http.dart' as http;

class LoopInfo extends StatefulWidget {
  Loop loop;
  Race race;

  LoopInfo(this.loop, this.race);

  @override
  _LoopInfoState createState() => _LoopInfoState(loop, race);
}

class _LoopInfoState extends State<LoopInfo> {
  Loop loop;
  Race race;

  _LoopInfoState(this.loop, this.race);

  List<LapsTimes> lapsList = new List();
  List<Racer> raceList = new List();
  bool doneRaces = false;

  get_laps() {
    DBProvider.db.LapsTimes_list(loop.id).then((data) {
      setState(() {
        lapsList = data;
        print(data);
      });
    });
  }

  void getRacers() async {
    print(loop.id);
    try {
      await http.post('$url/view_racer_in_loop.php', body: {
        'Id_loop': loop.id.toString(),
      }).then((data) async {
        List dataServer = await jsonDecode(data.body);
        print(dataServer);
        raceList.clear();
        dataServer.forEach((element) {
          Racer racer = Racer.fromJSONserver(element);
          print(racer.toString());
          setState(() {
            raceList.add(racer);
          });
        });
        setState(() {
          doneRaces = true;
        });
        print("/////////////////////////////");
        print(raceList);
      });
    } catch (ex) {
//      Toast.show("Try Again !", context,
//          backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);

      print(ex);
    }
  }

  @override
  void initState() {
    get_laps();
    getRacers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: Container(
          height: 150,
          child: Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Stack(
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image:
                                            AssetImage('Images/LaLigaLogo.png'),
                                        fit: BoxFit.fill)),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 16.0, top: 8.0, bottom: 8.0),
                              child: SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      race.name,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w100),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 16.0),
                                      child: Text(
                                        "Info ${loop.name}",
                                        style: TextStyle(
                                            color: Colors.white54,
                                            fontSize: 20,
                                            fontWeight: FontWeight.w100),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            color: Colors.blue,
                            height: 2,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 350,
              color: Color(0xff121214),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 3.0),
                        child: Text(
                          "Starting time: ${loop.date_start.substring(10, 16)}",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.w100),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 3.0),
                        child: Text(
                          "Distance: ${loop.total_distance} km",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.w100),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 0.0, bottom: 3.0),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Mellantider: ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontWeight: FontWeight.w100),
                            ),
                            Row(
                              children: List.generate(lapsList.length, (index) {
                                return Row(
                                  children: <Widget>[
                                    Text(
                                      "${lapsList[index].distance.toString()} km",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 17,
                                          fontWeight: FontWeight.w100),
                                    ),
                                    index == lapsList.length - 1
                                        ? Container()
                                        : Text(
                                            ", ",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17,
                                                fontWeight: FontWeight.w100),
                                          )
                                  ],
                                );
                              }),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Checka in-racers:",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.w100),
                        ),
                      ),
                      doneRaces
                          ? Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, bottom: 3.0),
                              child: raceList.length == 0
                                  ? Padding(
                                      padding: const EdgeInsets.only(top: 16.0),
                                      child: Center(
                                        child: Text(
                                          "No Racers !!",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17,
                                              fontWeight: FontWeight.w100),
                                        ),
                                      ),
                                    )
                                  : Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: List.generate(raceList.length,
                                          (index) {
                                        return Padding(
                                          padding:
                                              const EdgeInsets.only(top: 8.0),
                                          child: Text(
                                            "${raceList[index].first_name.toString()} ${raceList[index].last_name.toString()} ",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17,
                                                fontWeight: FontWeight.w100),
                                          ),
                                        );
                                      }),
                                    ),
                            )
                          : raceList.length != 0
                              ? Padding(
                                  padding: const EdgeInsets.only(top: 16.0),
                                  child: Center(
                                    child: Text(
                                      "No Racers !!",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 17,
                                          fontWeight: FontWeight.w100),
                                    ),
                                  ),
                                )
                              : Padding(
                                  padding: EdgeInsets.only(top: 24.0),
                                  child: SpinKitThreeBounce(
                                    size: 17,
                                    color: Colors.blue,
                                  ),
                                )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 16.0, left: 8.0),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: InkWell(
                onTap: () => Navigator.pop(context),
                child: Text(
                  "Back",
                  style: TextStyle(
                      color: Colors.blue,
                      fontSize: 15,
                      fontWeight: FontWeight.w300),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
