import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:manager_finish/Race/Race.dart';
import 'package:manager_finish/User/Manager.dart';

import 'package:toast/toast.dart';

import '../ControlPage.dart';
import '../Database.dart';
import '../LiveResults.dart';
import '../User/Login.dart';
import 'Loop.dart';

class LoopsScreen extends StatefulWidget {
  Race race;

  LoopsScreen(this.race);

  @override
  _LoopsScreenState createState() => _LoopsScreenState(race);
}

class _LoopsScreenState extends State<LoopsScreen> {
  Race race;

  _LoopsScreenState(this.race);

  ScrollController _scrollController;
  List<Loop> loops = new List();
  List<Manager> manager_list = new List();

  @override
  void initState() {
    _scrollController = ScrollController();
    DBProvider.db.Manager_list().then((value) {
      if(value.length>0){
        setState(() {
          manager_list = value;
        });
      }

    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: Container(
          height: 175,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 24.0),
              child: Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Stack(
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.of(context)
                                          .pushNamedAndRemoveUntil(
                                              '/RacesScreen',
                                              (Route<dynamic> route) => false);
                                    },
                                    child: Container(
                                      width: 70,
                                      height: 50,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  'Images/LaLigaLogo.png'),
                                              fit: BoxFit.fill)),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.vertical,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          //color: Colors.white,
                                          width: 200,
                                          child: Center(
                                            child: Text(
                                              race.name,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w100),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 16.0),
                                          child: Container(
                                            width: 200,
                                            child: Center(
                                              child: Text(
                                                race.location,
                                                style: TextStyle(
                                                    color: Colors.white54,
                                                    fontSize: 15,
                                                    fontWeight:
                                                        FontWeight.w100),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              color: Colors.blue,
                              height: 2,
                            ),
                          )
                        ],
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 12.0, top: 0.0),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(bottom: 16.0),
                                child: InkWell(
                                  onTap: () {
                                    manager_list.length== 0
                                        ? Navigator.push(
                                            context,
                                            new MaterialPageRoute(
                                                builder: (context) =>
                                                    Login(race)))
                                        : Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ControlPage(race)));
                                  },
                                  child: Container(
                                    width: 20,
                                    height: 25,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: manager_list.length==0
                                                ? AssetImage(
                                                    'Images/profile.png')
                                                : AssetImage(
                                                    'Images/greenProfile.png'),
                                            fit: BoxFit.fill)),
                                  ),
                                ),
                              ),
                              Container(
                                width: 25,
                                height: 25,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'Images/greyChecked.png'),
                                        fit: BoxFit.fill)),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
      body: Column(
        children: <Widget>[
          Expanded(
            child: loopsListView(),
          ),
          Container(
            color: Colors.black,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Colors.blue,
                    ),
                    width: 280,
                    height: 40,
                    child: InkWell(
                      onTap: () {
//                        Navigator.push(
//                            context,
//                            new MaterialPageRoute(
//                                builder: (context) =>
//                                    LiveResults(race, loops[0])));
                      },
                      child: Center(
                        child: Text(
                          "Liveresultat",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              fontWeight: FontWeight.w100),
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0, left: 8.0),
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Tillbaka",
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: 15,
                            fontWeight: FontWeight.w100),
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  FutureBuilder<List<Loop>> loopsListView() {
    return FutureBuilder(
      initialData: [],
      future: DBProvider.db.Loops_list(race.id),
      builder: (BuildContext context, AsyncSnapshot<List<Loop>> snapshot) {
        if (snapshot.hasData && snapshot.data.length > 0) {
          return ListView.builder(
              itemCount: snapshot.data.length,
              controller: _scrollController,
              itemBuilder: (BuildContext context, int index) {
                snapshot.data.sort((a,b) => a.date_start.compareTo(b.date_start));
                Loop item = snapshot.data[index];

                return Padding(
                  padding: const EdgeInsets.all(1.0),
                  child: InkWell(
                    onTap: () {
                      manager_list.length==0?
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  Login(race))):Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  ControlPage(race)));
                    },
                    child: Container(
                      color: Color(0xff121214),
                      height: 90,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Container(
                                    width: 275,
                                    child: Text(
                                      item.name,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15,
                                          fontWeight: FontWeight.w100),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, top: 2.0),
                                  child: Container(
                                    child: Row(
                                      children: <Widget>[
                                        Text(
                                          "Start ${item.date_start.substring(10, 16)} ,",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 15,
                                              fontWeight: FontWeight.w100),
                                        ),
                                        Text(
                                          "  ${item.total_distance} km",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 15,
                                              fontWeight: FontWeight.w100),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 16.0),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white70,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              });
        } else if (snapshot == null) {
          return Center(
              child: SpinKitThreeBounce(
            size: 17,
            color: Colors.blue,
          ));
        } else if (snapshot.data.length == 0) {
          return Center(
              child: Text(
            "No Loops Here",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300),
          ));
        }

        return Center(
          child: CircularProgressIndicator(
            backgroundColor: Colors.green,
          ),
        );
      },
    );
  }
}
