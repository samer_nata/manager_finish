import 'package:flutter/material.dart';
import 'package:manager_finish/Loop/LapsTimes.dart';

import 'Loop/Loop.dart';
import 'Race/Race.dart';

class LiveResults extends StatefulWidget {
  Race race;
  Loop loop;
LapsTimes lapsTimes;
  LiveResults(this.race, this.loop,this.lapsTimes);

  @override
  _LiveResultsState createState() => _LiveResultsState(race);
}

class _LiveResultsState extends State<LiveResults> {
  Race race;
  Loop loop;
  LapsTimes lapsTimes;

  _LiveResultsState(this.race);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          child: Container(
            height: 175,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 24.0),
                child: Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: InkWell(
                                  onTap: () {
                                    Navigator.of(context)
                                        .pushNamedAndRemoveUntil('/RacesScreen',
                                            (Route<dynamic> route) => false);
                                  },
                                  child: Container(
                                    width: 70,
                                    height: 50,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'Images/LaLigaLogo.png'),
                                            fit: BoxFit.fill)),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.vertical,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        //color: Colors.white,
                                        width: 200,
                                        child: Center(
                                          child: Text(
                                            race.name,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15,
                                                fontWeight: FontWeight.w100),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 8.0),
                                        child: Container(
                                          //color: Colors.white,
                                          width: 200,
                                          child: Center(
                                            child: Text(
                                              loop.name,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w100),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(bottom: 16.0),
                                      child: Container(
                                        width: 20,
                                        height: 25,
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: AssetImage(
                                                    'Images/greenProfile.png'),
                                                fit: BoxFit.fill)),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            color: Colors.blue,
                            height: 2,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          preferredSize: Size.fromHeight(120),
        ),
        backgroundColor: Colors.black,
        body: Stack(
          children: <Widget>[
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 240,
                    color: Color(0xff121214),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 16.0, left: 8.0),
                            child: Text(
                              "Starttid: 19:00",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w100),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: Text(
                              "Distans: 10 km",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w100),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: Text(
                              "Mellantider: 2.3 km (m), 4.2 km (m), 5.6 km, 7.4 km, 8,7 km (m).",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w100),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: Text(
                              "Stefan EKSTRÖM, 232, OK Kullingshof",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w100),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: Text(
                              "David MALM, 233, OK Stigen",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w100),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: Text(
                              "Evert BERGSTRÖM, 234, SK Hol",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w100),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 24.0, left: 16.0),
              child: Align(
                alignment: Alignment.bottomLeft,
                child: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Text(
                    "Tillbaka",
                    style: TextStyle(
                        color: Colors.blue,
                        fontSize: 15,
                        fontWeight: FontWeight.w300),
                  ),
                ),
              ),
            )
          ],
        ));
  }
}
