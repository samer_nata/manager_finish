import 'package:flutter/material.dart';
import 'package:manager_finish/ControlPage.dart';
import 'package:manager_finish/Loop/LoopsScreen.dart';
import 'package:manager_finish/Race/Race.dart';
import 'Database.dart';
import 'Race/RacesScreen.dart';
import 'SplashScreen.dart';


void main() {
  runApp(MyApp());
  DBProvider.db.database;
  DBProvider.db.open();
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var key = GlobalKey();
  /*****samer****/

  @override
  Widget build(BuildContext context) {


    return MaterialApp(
      key: key,
      debugShowCheckedModeBanner: false,
      routes: {
        '/RacesScreen': (context) => RacesScreen(),
        '/Control':(context)=>ControlPage(  ModalRoute.of(context).settings.arguments),
        "/loops":(context)=>LoopsScreen(ModalRoute.of(context).settings.arguments),

      },
      initialRoute: '/',
      home: SplashScreen(),
    );
  }
}
