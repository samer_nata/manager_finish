class Race {
  int id;
  String name;
  String location;
  String date_race;
  bool state=false;
  bool is_start=true;

  Race({this.id, this.name, this.location, this.date_race, this.state,this.is_start});

  factory Race.fromJSON(json) {
    return Race(
        id: json["id"],
        name: json["name"],
        date_race: json["date_race"],
        location: json["location"],
        state: json["state"] == 0 ? false : true,is_start: json["is_start"] == 0 ? false : true);
  }

  factory Race.fromJSONserver(json) {
    return Race(
        id: int.parse(json["id"]),
        name: json["name"],
        date_race: json["date_race"],
        location: json["location"],
        state: json["state"] == "0" ? false : true,is_start: json["is_start"] == "0" ? false : true);
  }

  Map<String, dynamic> toMap() => {
    "id": id,
    "name": name,
    "date_race": date_race,
    "state": state == true ? 1 : 0,
    "location": location,    "is_start": state == true ? 1 : 0,

  };

  @override
  String toString() {
    return 'Race{id: $id, name: $name, location: $location, date_race: $date_race, state: $state}';
  }
}
