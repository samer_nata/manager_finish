import 'package:flutter/material.dart';


import 'Loop/Loop.dart';
import 'OurTabBarView/Results.dart';
import 'OurTabBarView/Record.dart';

import 'OurTabBarView/Start.dart';
import 'Race/Race.dart';

class ControlPage extends StatefulWidget {
  Race race;

/***************/
  ControlPage(this.race);

  @override
  _ControlPageState createState() => _ControlPageState(race);
}

class _ControlPageState extends State<ControlPage>
    with SingleTickerProviderStateMixin {

  TabController tabController;
  Race race;

  _ControlPageState(this.race);

  @override
  void initState() {
    tabController = new TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: new AppBar(
        title: Text(
          race.name,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w300, fontSize: 18),
        ),
        centerTitle: true,
        leading: Padding(
          padding: const EdgeInsets.only(top: 8.0, left: 8.0),
          child: InkWell(
            onTap: () {
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/RacesScreen', (Route<dynamic> route) => false);
            },
            child: Container(
              width: 70,
              height: 50,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('Images/LaLigaLogo.png'),
                      fit: BoxFit.fill)),
            ),
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: 20,
              height: 25,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('Images/greenProfile.png'),
                      fit: BoxFit.scaleDown)),
            ),
          ),
        ],
        backgroundColor: Colors.black,
        bottom: TabBar(
          unselectedLabelColor: Colors.white,
          indicatorColor: Colors.blue,
          labelColor: Colors.blue,
          controller: tabController,
          indicatorWeight: 0.9,
          tabs: <Widget>[
            Text(
              "Start",
              style: TextStyle(fontWeight: FontWeight.w300, fontSize: 18),
            ),
            Text(
              "Tidtagning",
              style: TextStyle(fontWeight: FontWeight.w300, fontSize: 18),
            ),
            Text(
              "Resultat",
              style: TextStyle(fontWeight: FontWeight.w300, fontSize: 18),
            ),
          ],
        ),
      ),
      body: TabBarView(controller: tabController, children: <Widget>[
        new Container(
          child: Start(race),
        ),
        new Container(
          child: Record(race),
        ),
        new Container(
          child:
          Results(race),
        ),
      ]),
    );
  }
}
