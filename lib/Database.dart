import 'dart:convert';
import 'dart:io';


import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

import 'package:flutter/material.dart';

import 'Loop/LapsTimes.dart';
import 'Loop/Loop.dart';
import 'Note.dart';
import 'Race/Race.dart';
import 'User/Manager.dart';
import 'User/Racer.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDB();
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "race_manager_new.db");
    return await openDatabase(path, version: 2,
        onCreate: (Database db, int version) async {
      await db.execute("PRAGMA foreign_keys = ON;");
//          await db.execute("COMMIT;");
//      await db.execute(" PRAGMA foreign_keys=on; COMMIT;");
      /* await db.execute(
          "CREATE TABLE Currency (Currency_id	INTEGER PRIMARY KEY AUTOINCREMENT not null ,Currency_name TEXT UNIQUE not null);");*/
      await db.execute(
          "CREATE TABLE Race (id int PRIMARY KEY  not null,name TEXT not null,location TEXT not null,date_race TEXT not null,state BOOLEAN,is_start BOOLEAN);");
      await db.execute(
          "CREATE TABLE loops( id int PRIMARY KEY not null,name TEXT not null,date_start TEXT not null,total_distance REAL, X  REAL NOT NULL , Y   REAL not null,id_race int not null,state BOOLEAN, is_start Boolean,FOREIGN KEY (id_race) REFERENCES Race(id) on DELETE CASCADE on update CASCADE );");
      await db.execute(
          "CREATE TABLE lap_times (id int PRIMARY key not null,distance REAL ,id_loop int not null,FOREIGN KEY (id_loop) REFERENCES loops(id)  on DELETE CASCADE on update CASCADE);");
      await db.execute(
          "CREATE TABLE Manager (id int PRIMARY KEY not null,email TEXT ,password TEXT);");
      await db.execute(
          "CREATE TABLE Racer (id int PRIMARY KEY not null,first_name TEXT not null,last_name TEXT not null,pip int not null,club TEXT not null,division TEXT not null);");
          await db.execute(
              "CREATE TABLE Note (containe TEXT);");
    });
  }

  open() async {
    if (_database != null) {
      await openDatabase("race_manager_new.db");
//      await updateall_bill_state_false();
//    await updateall_department_state_false();
//      await   DBProvider.db.close_app();

    }
  }

  Future<void> close() async {
//    await refresh_state_app();
    await _database.close();
  }

/**********************************************************************************/
  Future<void> insert_note(Note  note) async {
//    print(department.toString());
//    department.count_bill_in_dept=await count_bills(department.Department_id);
print(note.containe);
    await _database.insert(
      'Note',
      note.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> deleteAllNote() async {
    final db = await database;

    await db.delete("Note");
    return;
  }

  Future<List<Note>> Note_all() async {
    final db = await database;
    List<Map<String, dynamic>> maps =
    await db.query('Note');
    print(maps);
    return List.generate(maps.length, (i) {
      return Note.fromJSON(maps[i]);
    });
  }
  /**********************Race*****************************************************************************************/

  Future<void> insert_race(Race race) async {
//    print(department.toString());
//    department.count_bill_in_dept=await count_bills(department.Department_id);
    await _database.insert(
      'Race',
      race.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }


  Future<void> update_race(Race race, int id) async {
//    print(department.toString());
//    department.count_bill_in_dept=await count_bills(department.Department_id);
    await _database.update(
      'Race',
      race.toMap(),
      where: "id = ?",
      whereArgs: [id],
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }
  insertallRace(List listrace) async {
//    print(">>>>>>>>>>>>>>>>>>>>>>> $listDepartment");
    final db1 = await database;

    await db1.transaction((db) async {
      listrace.forEach((obj) async {
//        print("befor   : ${obj.toString()}");
        Race race = new Race.fromJSONserver(obj);
//        print("after    : ${race.toString()}");

        try {
          db.insert('Race', race.toMap(),
              conflictAlgorithm: ConflictAlgorithm.replace);
        } catch (ex) {}
      });
    });
    return;
  }

  Future<List<Race>> Race_list_state_true() async {
    final db = await database;
    List<Map<String, dynamic>> maps =
        await db.query('Race', where: "state <> 0");
//    print(maps);
    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }

  Future<List<Race>> Race_list_false() async {
    final db = await database;
    List<Map<String, dynamic>> maps =
        await db.query('Race', where: "state = 0 ");
//    print(maps);
    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }
  Future<List<Race>> Race_list_all() async {
    final db = await database;
    List<Map<String, dynamic>> maps =
    await db.query('Race');
//    print(maps);
    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }
  Future<void> deleteAllRace() async {
    final db = await database;

    await db.delete("Race");
    return;
  }

//

  /******************************************end race***************************************************/
  /*****************************Racer **********************************************/
  Future<void> insert_racer(Racer racer) async {
//    print(department.toString());
//    department.count_bill_in_dept=await count_bills(department.Department_id);
    await _database.insert(
      'Racer',
      racer.to_Map(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  insertallRacer(List listracer) async {
//    print(">>>>>>>>>>>>>>>>>>>>>>> $listDepartment");
    final db1 = await database;

    await db1.transaction((db) async {
      listracer.forEach((obj) async {
       print("befor   : ${obj.toString()}");
        Racer race = new Racer.fromJSONserver(obj);
        print("after    : ${race.toString()}");

        try {
          db.insert('Racer', race.to_Map(),
              conflictAlgorithm: ConflictAlgorithm.replace);
        } catch (ex) {}
      });
    });
    return;
  }

  Future<List<Racer>> Racer_list() async {
    final db = await database;
    List<Map<String, dynamic>> maps =
    await db.query('Racer');
//    print(maps);
    return List.generate(maps.length, (i) {
      return Racer.fromJSON(maps[i]);
    });
  }

  Future<List<Racer>> Racer_list_without() async {
    final db = await database;
    List<Map<String, dynamic>> maps =
    await db.query('Racer',groupBy: 'division');
//    print(maps);
    return List.generate(maps.length, (i) {
      return Racer.fromJSON(maps[i]);
    });
  }

  Future<void> deleteAllRacer() async {
    final db = await database;

    await db.delete("Racer");
    return;
  }
  /***************************End Racer************************************************/
  Future<void> insert_manager(Manager manager) async {
//    print(department.toString());
//    department.count_bill_in_dept=await count_bills(department.Department_id);
    await _database.insert(
      'Manager',
      manager.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Manager>> Manager_list() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Manager');
//    print(maps);
    return List.generate(maps.length, (i) {
      return Manager.fromJSON(maps[i]);
    });
  }

  Future<void> deleteAllManager() async {
    final db = await database;

    await db.delete("Manager");
    return;
  }

  /**********************************end manager ****************************************************/

  /**************************************loops***************************************************************/

  Future<void> insert_loops(Loop loop) async {
//    print(department.toString());
//    department.count_bill_in_dept=await count_bills(department.Department_id);
    await _database.insert('loops', loop.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace, nullColumnHack: "state");
  }

  Future<void> update_loops(Loop loop, int id) async {
//    print(department.toString());
//    department.count_bill_in_dept=await count_bills(department.Department_id);
    await _database.update(
      'loops',
      loop.toMap(),
      where: "id = ?",
      whereArgs: [id],
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  insertallLoops(List listrace) async {
//    print(">>>>>>>>>>>>>>>>>>>>>>> $listDepartment");
    final db1 = await database;

    await db1.transaction((db) async {
      listrace.forEach((obj) async {
//        print("befor   : ${obj.toString()}");
        Loop loop = new Loop.fromJSONserver(obj);
//        print("after    : ${loop.toString()}");

        try {
          db.insert('loops', loop.toMap(),
              nullColumnHack: 'state',
              conflictAlgorithm: ConflictAlgorithm.replace);
        } catch (ex) {}
      });
    });
    return;
  }

  Future<List<Loop>> Loops_list(int id_race) async {
    final db = await database;
    List<Map<String, dynamic>> maps =
        await db.query('loops', where: "id_race = ?", whereArgs: [id_race]);
//    print(maps);
    return List.generate(maps.length, (i) {
      return Loop.fromJson(maps[i]);
    });
  }

  Future<List<Loop>> Loops_list_all() async {
    final db = await database;
    List<Map<String, dynamic>> maps =
    await db.query('loops',);
//    print(maps);
    return List.generate(maps.length, (i) {
      return Loop.fromJson(maps[i]);
    });
  }

  Future<void> deleteAllLoops() async {
    final db = await database;

    await db.delete("loops");
    return;
  }

  /******************************************lap_times*********************************************************/
  Future<void> insert_lap_times(LapsTimes lapsTimes) async {
//    print(department.toString());
//    department.count_bill_in_dept=await count_bills(department.Department_id);
    await _database.insert(
      'lap_times',
      lapsTimes.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  insertall_lapsTimes(List listrace) async {
//    print(">>>>>>>>>>>>>>>>>>>>>>> $listDepartment");
    final db1 = await database;

    await db1.transaction((db) async {
      listrace.forEach((obj) async {
//        print("befor   : ${obj.toString()}");
        LapsTimes lap_times = new LapsTimes.fromJSONserver(obj);

        try {
          db.insert('lap_times', lap_times.toMap(),
              conflictAlgorithm: ConflictAlgorithm.replace);
        } catch (ex) {}
      });
    });
    return;
  }

  Future<List<LapsTimes>> LapsTimes_list(int id_loops) async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db
        .query('lap_times', where: "id_loop = ?", whereArgs: [id_loops]);
//    print(maps);
    return List.generate(maps.length, (i) {
      return LapsTimes.fromJSON(maps[i]);
    });
  }
  Future<List<LapsTimes>> LapsTimes_list_all() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db
        .query('lap_times');
//    print(maps);
    return List.generate(maps.length, (i) {
      return LapsTimes.fromJSON(maps[i]);
    });
  }


  Future<List<LapsTimes>> LapsTimes_from_race_list(int id_race) async {

    final db = await database;
    List<Map<String, dynamic>> maps = await db
        .query('lap_times', where: "id_loop in (Select id from loops where id_race = ${id_race} )");
//    print(maps);
    return List.generate(maps.length, (i) {
      return LapsTimes.fromJSON(maps[i]);
    });
  }





  Future<void> deleteAllLapsTimes() async {
    final db = await database;

    await db.delete("lap_times");
    return;
  }
}
